from rplidar import RPLidar
from scanf import scanf
from turtle import *
from math import cos, sin, pi, radians, floor
import numpy as np

def lidar_start():
    lidar = RPLidar('/dev/ttyUSB0')
    info = lidar.get_info()
    print(info)
    health = lidar.get_health()
    print(health)
    lidar.start_motor()
    return lidar

def lidar_get(lidar):
    print('Scanning\n')
    data = np.zeros((360, 2))
    for i, scan in enumerate(lidar.iter_scans()):
        for measure in scan:
            quality = measure[0]
            angle = floor(measure[1])
            dist = floor(measure[2])
            if data[angle, 0] == 0:
                data[angle, 0] = dist
                data[angle, 1] = quality
            else:
                if quality >= data[angle, 1]:
                    data[angle, 0] = dist
                    data[angle, 1] = quality
        if i >= 10:
            break
    lidar.stop()
    return data[:, 0]

def lidar_stop(lidar):
    lidar.stop_motor()
    lidar.disconnect()

def lidar_trace(cord):
    for i in range(len(cord)):
        print("angle=", i, " dist=", cord[i])
#    down()
#    for i in range(0,360):
#        if cord[i][0] != 0:
#            dist = cord[i][0] / cord[i][1]
#            goto(dist/10*cos(radians(i)),dist/10*sin(radians(i)))

def main():
    lidar = lidar_start();
    try:
        while(1):
            cord = lidar_get(lidar);
            lidar_trace(cord);
    except KeyboardInterrupt:
        print('Stoping.')

    lidar_stop(lidar)

if __name__ == '__main__':
    main()
