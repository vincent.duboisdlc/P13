SLAM Mapping
============

## Installation

### Installation de BreezySLAM pour Python
<pre>
sudo python3 sources/python/setup.py install
</pre>
source: <a href="https://github.com/simondlevy/BreezySLAM">BreezySLAM</a> by Simon D. Levy

## Installation du logiciel pour visualiser la cartographie
<pre>
sudo python3 sources/PyRobotViz/setup.py install
</pre>
source: <a href="https://github.com/simondlevy/PyRoboViz">PyRoboViz</a> by Simon D. Levy
