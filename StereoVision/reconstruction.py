import cv2
import numpy as np
import glob
from tqdm import tqdm
import PIL.ExifTags
import PIL.Image
from matplotlib import pyplot as plt

live = False

#Function to create point cloud file
def create_output(vertices, colors, filename):
	colors = colors.reshape(-1,3)
	vertices = np.hstack([vertices.reshape(-1,3),colors])
	ply_header = '''ply
		format ascii 1.0
		element vertex %(vert_num)d
		property float x
		property float y
		property float z
		property uchar red
		property uchar green
		property uchar blue
		end_header
		'''
	with open(filename, 'w') as f:
		f.write(ply_header %dict(vert_num=len(vertices)))
		np.savetxt(f,vertices,'%f %f %f %d %d %d')

###################################################
######Prepare camera and disparity parameters######
###################################################

#Load camera parameters
KR = np.load('./CalibCam/camera_params/K_right.npy')
KL = np.load('./CalibCam/camera_params/K_left.npy')
distR = np.load('./CalibCam/camera_params/dist_right.npy')
distL = np.load('./CalibCam/camera_params/dist_left.npy')
ROTR = np.load('./CalibCam/camera_params/ROT_right.npy')
ROTL = np.load('./CalibCam/camera_params/ROT_left.npy')
TRANSR = np.load('./CalibCam/camera_params/TRANS_right.npy')
TRANSL = np.load('./CalibCam/camera_params/TRANS_left.npy')

#Specify image paths
pathR = './stereoImages/right.jpg'
pathL = './stereoImages/left.jpg'

#Get height and width. Note: It assumes that both pictures are the same size.
h,w = cv2.imread(pathR).shape[:2]

#Get optimal camera matrix for better undistortion
new_camera_matrixR, roiR = cv2.getOptimalNewCameraMatrix(KR,distR,(w,h),1,(w,h))
new_camera_matrixL, roiL = cv2.getOptimalNewCameraMatrix(KL,distL,(w,h),1,(w,h))

#Create Block matching object.
win_size = 2808 #2
stereo = cv2.StereoSGBM_create(
	minDisparity = 0,
	numDisparities = 160,
	blockSize = 1,
	uniquenessRatio = 8, #5
	speckleWindowSize = 238, #40
	speckleRange = 1, #20
	disp12MaxDiff = 0,
	preFilterCap = 0,
	P1 = 8*2*win_size**2,
	P2 = 32*2*win_size**2
)

if live:
	camR= cv2.VideoCapture(1)
	camL= cv2.VideoCapture(2)

while True:
	if live:
		retR, frameR = camR.read()
		retL, frameL = camL.read()
	else:
		frameR = cv2.imread(pathR)
		frameL = cv2.imread(pathL)
	#Undistort images
	imgR = cv2.undistort(frameR, KR, distR, None, new_camera_matrixR)
	imgL = cv2.undistort(frameL, KL, distL, None, new_camera_matrixL)

	#Compute disparity map
	#print ("\nComputing the disparity  map...")
	disparity_map = stereo.compute(imgR, imgL)

	#Show disparity map before generating 3D cloud to verify that point cloud will be usable.
	plt.imshow(disparity_map,'gray')
	plt.show()

	key = cv2.waitKey(1) & 0xFF
	if key == ord('q'):
	    break
	if key == ord(' '):
	    continue

if live:
	camR.release()
	camL.release()
cv2.destroyAllWindows()

################################
######Gereate points cloud######
################################
h,w = imgR.shape[:2] #Get new downsampled width and height

focal_length = 4.2 #Load focal length     a verifier

output_file = '3D.ply'

print("gererating point cloud" + output_file);

"""
#Trouver la matrice Q :

RL=np.float32([[0,0,0],[0,0,0],[0,0,0]])
RR=np.float32([[0,0,0],[0,0,0],[0,0,0]])
PL=np.float32([[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
PR=np.float32([[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
Q=np.float32([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])

cv2.stereoRectify(K1, K2, dist1, dist2, img_1_undistorted.shape[:2], ROT1, TRANS1, R1, R2, P1, P2, Q, flags=0, alpha=-1)

#RL, RR, PL, PR, Q = cv2.stereoRectify(K2, dist2, K1, dist1, img_1_undistorted.shape[:2], ROT1, TRANS1, alpha=-1)
"""

Q = np.float32([[1,0,0,0],
				[0,-1,0,0],
				[0,0,focal_length*0.05,0],
				[0,0,0,1]])

points_3D = cv2.reprojectImageTo3D(disparity_map, Q) #Reproject points into 3D

colors = cv2.cvtColor(img_1_undistorted, cv2.COLOR_BGR2RGB)#Get color points

mask_map = disparity_map > disparity_map.min() #Get rid of points with value 0 (i.e no depth)

#Mask colors and points.
output_points = points_3D[mask_map]
output_colors = colors[mask_map]

#Generate point cloud
print ("\n Creating the output file... \n")
create_output(output_points, output_colors, output_file)
