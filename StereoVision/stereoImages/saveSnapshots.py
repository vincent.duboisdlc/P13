import cv2
import time
import sys
import argparse
import os


cap1 = cv2.VideoCapture(1)
cap2 = cv2.VideoCapture(2)

cap1.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap1.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
cap2.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap2.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

#w = cap1.get(cv2.CAP_PROP_FRAME_WIDTH)
#h = cap1.get(cv2.CAP_PROP_FRAME_HEIGHT)

fileName1="./right"
fileName2="./left"

i=0

while True:
    ret1, frame1 = cap1.read()
    ret2, frame2 = cap2.read()

    cv2.namedWindow("Right", cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow("Left", cv2.WINDOW_AUTOSIZE)
    cv2.moveWindow("Left", 150, 200)
    cv2.moveWindow("Right", 150+640, 200)
    cv2.imshow("Right", frame1)
    cv2.imshow("Left", frame2)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    if key == ord(' '):
        #print("Saving image ", nSnap)
        #cv2.imwrite("%s.jpg"%(fileName1), frame1)
        #cv2.imwrite("%s.jpg"%(fileName2), frame2)
        cv2.imwrite("%s.jpg"%(fileName1+str(i)), frame1)
    i+=1

cap1.release()
cap2.release()
cv2.destroyAllWindows()
