import cv2
import numpy as np
import glob
from tqdm import tqdm
import PIL.ExifTags
import PIL.Image
from matplotlib import pyplot as plt

#Function to create point cloud file
def create_output(vertices, colors, filename):
	colors = colors.reshape(-1,3)
	vertices = np.hstack([vertices.reshape(-1,3),colors])

	ply_header = '''ply
		format ascii 1.0
		element vertex %(vert_num)d
		property float x
		property float y
		property float z
		property uchar red
		property uchar green
		property uchar blue
		end_header
		'''
	with open(filename, 'w') as f:
		f.write(ply_header %dict(vert_num=len(vertices)))
		np.savetxt(f,vertices,'%f %f %f %d %d %d')

#Load camera parameterspip install opencv-contrib-python
KR = np.load('../CalibCam/camera_params/K_right.npy')
KL = np.load('../CalibCam/camera_params/K_left.npy')
distR = np.load('../CalibCam/camera_params/dist_right.npy')
distL = np.load('../CalibCam/camera_params/dist_left.npy')

#Specify image paths
pathR = '../stereoImages/right.jpg'
pathL = '../stereoImages/left.jpg'

#Load pictures
imgR = cv2.imread(pathR)
imgL = cv2.imread(pathL)
cv2.namedWindow("Right", cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("Left", cv2.WINDOW_AUTOSIZE)
cv2.moveWindow("Right", 1200, 1)
cv2.moveWindow("Left", 1200, 200+480)
cv2.imshow("Right", imgR)
cv2.imshow("Left", imgL)

#Get height and width. Note: It assumes that both pictures are the same size. They HAVE to be same size and height.
h,w = imgR.shape[:2]

#Get optimal camera matrix for better undistortion
new_camera_matrix1, roi1 = cv2.getOptimalNewCameraMatrix(KR,distR,(w,h),1,(w,h))
new_camera_matrix2, roi2 = cv2.getOptimalNewCameraMatrix(KL,distL,(w,h),1,(w,h))

#Undistort images
imgR_undistorted = cv2.undistort(imgR, KR, distR, None, new_camera_matrix1)
imgL_undistorted = cv2.undistort(imgL, KL, distL, None, new_camera_matrix2)

cv2.namedWindow('TuneParameters') #creation de la fenetre

def nothing(i):
	#plt.clf()
	plt.close()
	return 0

#creation des trackbars
cv2.createTrackbar('minDisparity','TuneParameters', 0, 20, nothing)
cv2.setTrackbarPos('numDisparities', 'TuneParameters', 0)
cv2.createTrackbar('numDisparities', 'TuneParameters', 0, 20, nothing)
cv2.setTrackbarPos('numDisparities', 'TuneParameters', 4)
cv2.createTrackbar('blockSize', 'TuneParameters', 0, 11, nothing)
cv2.setTrackbarPos('blockSize', 'TuneParameters', 3)
cv2.createTrackbar('uniquenessRatio', 'TuneParameters', 3, 15, nothing)
cv2.setTrackbarPos('uniquenessRatio', 'TuneParameters', 5)
cv2.createTrackbar('speckleWindowSize', 'TuneParameters', 0, 500, nothing)
cv2.setTrackbarPos('speckleWindowSize', 'TuneParameters', 40)
cv2.createTrackbar('speckleRange', 'TuneParameters', 0, 100, nothing)
cv2.setTrackbarPos('speckleRange', 'TuneParameters', 20)
cv2.createTrackbar('disp12MaxDiff', 'TuneParameters', 0, 10, nothing)
cv2.setTrackbarPos('disp12MaxDiff', 'TuneParameters', 0)
cv2.createTrackbar('win_size', 'TuneParameters', 0, 10, nothing)
cv2.setTrackbarPos('win_size', 'TuneParameters', 2)


while True:
	min_disparity = cv2.getTrackbarPos('minDisparity', 'TuneParameters')
	num_disparities = cv2.getTrackbarPos('numDisparities', 'TuneParameters')
	block_size = cv2.getTrackbarPos('blockSize', 'TuneParameters')
	uniqueness_ratio = cv2.getTrackbarPos('uniquenessRatio', 'TuneParameters')
	speckle_windowSize = cv2.getTrackbarPos('speckleWindowSize', 'TuneParameters')
	speckle_range = cv2.getTrackbarPos('speckleRange', 'TuneParameters')
	disp12_maxDiff = cv2.getTrackbarPos('disp12MaxDiff', 'TuneParameters')
	win_size = cv2.getTrackbarPos('win_size', 'TuneParameters')

	stereo = cv2.StereoSGBM_create(
		minDisparity = min_disparity,
		numDisparities  = num_disparities*16,
		blockSize = block_size,
		uniquenessRatio = uniqueness_ratio,
		speckleWindowSize = speckle_windowSize*10,
		speckleRange = speckle_range*10,
		disp12MaxDiff = disp12_maxDiff,
		preFilterCap = 0,
		P1 = 8*2*win_size**2,
		P2 = 32*2*win_size**2
	)

	#disparity = stereo.compute(imgL_undistorted, imgR_undistorted).astype(np.float32) / 16.0
    #disparity = (disparity-min_disp)/num_disp
	disparity_map = stereo.compute(imgR_undistorted, imgL_undistorted)

	try:
		plt.imshow(disparity_map,'gray')
		plt.show()
	except KeyboardInterrupt:
		print('\nStoping')


cv2.destroyAllWindows()
print ("minDisparity", min_disparity)
print ("num_disparities", num_disparities*16)
print ("block_size", block_size)
print ("uniqueness_ratio", uniqueness_ratio)
print ("speckle_windowSize", speckle_windowSize)
print ("speckle_range", speckle_range)
print ("disp12_maxDiff", disp12_maxDiff)
print ("win_size", win_size)
