import numpy as np
import cv2
import glob
import sys
import argparse

#Parametres du damier
nRows = 9
nCols = 6
dimension = 25 #- en mm

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, dimension, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((nRows*nCols,3), np.float32)
objp[:,:2] = np.mgrid[0:nCols,0:nRows].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

# Find the images files
imageType       = 'jpg'
filename    = "./calibImages/right/*." + imageType
images      = glob.glob(filename)

nPatternFound = 0
imgNotGood = images[1]

for fname in images:
    if 'calibresult' in fname: continue
    #-- Read the file and convert in greyscale
    img     = cv2.imread(fname)
    gray    = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    print("Reading image ", fname)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (nCols,nRows),None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        print("Pattern found! Press ESC to skip or ENTER to accept")
        #--- Sometimes, Harris cornes fails with crappy pictures, so
        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)

        # Draw and display the corners
        cv2.drawChessboardCorners(img, (nCols,nRows), corners2,ret)
        cv2.imshow('img',img)
        # cv2.waitKey(0)
        k = cv2.waitKey(0) & 0xFF
        if k == 27: #-- ESC Button
            print("Image Skipped")
            imgNotGood = fname
            continue

        print("Image accepted")
        nPatternFound += 1
        objpoints.append(objp)
        imgpoints.append(corners2)

        # cv2.waitKey(0)
    else:
        imgNotGood = fname

cv2.destroyAllWindows()

if (nPatternFound > 1):
    print("Found %d good images" % (nPatternFound))
    ret, K, dist, ROT, TRANS = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

    #Save parameters into numpy file
    #np.save("./camera_params/ret", ret)
    np.save("./camera_params/K_right", K)
    np.save("./camera_params/dist_right", dist)
    np.save("./camera_params/ROT_right", ROT)
    np.save("./camera_params/TRANS_right", TRANS)

else:
    print("In order to calibrate you need at least 9 good pictures... try again")
