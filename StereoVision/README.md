StereoVision
============

## Camera calibration

## Matcher calibration
<pre>
python3 reconstruction.py
</pre>

## Run
<pre>
python3 reconstruction.py
</pre>

## Output
<pre>
meshlab 3D.ply
</pre>
