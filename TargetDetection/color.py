import cv2 as cv
import argparse

minH = 0
minS = 0
minV = 0
maxH = 255
maxS = 255
maxV = 255

def minH_trackbar(val):
    global minH
    global maxH
    minH = val
    minH = min(maxH-1, minH)
    cv.setTrackbarPos('min H', 'detection', minH)
def maxH_trackbar(val):
    global minH
    global maxH
    maxH = val
    maxH = max(maxH, minH+1)
    cv.setTrackbarPos('max H', 'detection', maxH)
def minS_trackbar(val):
    global minS
    global maxS
    minS = val
    minS = min(maxS-1, minS)
    cv.setTrackbarPos('min S', 'detection', minS)
def maxS_trackbar(val):
    global minS
    global maxS
    maxS = val
    maxS = max(maxS, minS+1)
    cv.setTrackbarPos('max S', 'detection', maxS)
def minV_trackbar(val):
    global minV
    global maxV
    minV = val
    minV = min(maxV-1, minV)
    cv.setTrackbarPos('min V', 'detection', minV)
def maxV_trackbar(val):
    global minV
    global maxV
    maxV = val
    maxV = max(maxV, minV+1)
    cv.setTrackbarPos('max V', 'detection', maxV)


cap = cv.VideoCapture(0)
cv.namedWindow('video')
cv.namedWindow('detection')
cv.createTrackbar('min H', 'detection' , minH, 255, minH_trackbar)
cv.createTrackbar('max H', 'detection' , maxH, 255, maxH_trackbar)
cv.createTrackbar('min S', 'detection' , minS, 255, minS_trackbar)
cv.createTrackbar('max S', 'detection' , maxS, 255, maxS_trackbar)
cv.createTrackbar('min V', 'detection' , minV, 255, minV_trackbar)
cv.createTrackbar('max V', 'detection' , maxV, 255, maxV_trackbar)

while True:
    ret, frame = cap.read()
    if frame is None:
        break
    frame_HSV = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    frame_threshold = cv.inRange(frame_HSV, (minH, minS, minV), (maxH, maxS, maxV))

    cv.imshow('video', frame)
    cv.imshow('detection', frame_threshold)

    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        break
