#include <Servo.h>
int val[10];
int rotv[5];
int incli = 70;
int inclid = 90 + incli;
int inclig = 90 - incli;
int x = 20;
int d = 1;
bool rot = true;
//droite
Servo servo0;
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
//gauche
Servo servo5;
Servo servo6;
Servo servo7;
Servo servo8;
Servo servo9;

void setup() {
  servo0.attach(2);
  servo1.attach(3);
  servo2.attach(4);
  servo3.attach(5);
  servo4.attach(6);
  servo5.attach(7);
  servo6.attach(8);
  servo7.attach(9);
  servo8.attach(10);
  servo9.attach(11);
  
  val[0] = inclid;
  val[1] = inclid + (x *0.6);
  val[2] = inclid - (x *0.6);
  val[3] = inclid;
  val[4] = inclid + (x *0.6);
  
  val[5] = inclig ;
  val[6] = inclig - (x *0.6);
  val[7] = inclig + (x *0.6);
  val[8] = inclig;
  val[9] = inclig - (x *0.6);
  
  sens_rotationrotv[0] = true;
  rotv[1] = false;
  rotv[2] = false;
  rotv[3] = true;
  rotv[4] = false;

  Serial.begin(9600);
}

void loop() {
  for(int i =0; i<5; i++){
    if(val[i] >= inclid + x || val[i] <= inclid - x){
      rotv[i] = !rotv[i];
    }
    if(rotv[i]){
      val[i] = val[i] + d;
      val[i+5] = val[i+5] - d;
    }
    else{
      val[i] = val[i] - d;
      val[i+5] = val[i+5] + d;
    }
  }
  servo0.write(val[0]);
  servo1.write(val[1]);
  servo2.write(val[2]);
  servo3.write(val[3]);
  servo4.write(val[4]);
  servo5.write(val[5]);
  servo6.write(val[6]);
  servo7.write(val[7]);
  servo8.write(val[8]);
  servo9.write(val[9]);
  delay(5);
}
