#include <avr/io.h>
#include <util/delay.h>

int translationDegreEnPosition (int i_degre)
{
  return i_degre*22+3000;
}

//ecriture de la position pour les pins OCnA
void OC1A(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR1A = ICR1 - i_position;
}

void OC3A(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR3A = ICR3 - i_position;
}

void OC4A(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR4A = ICR4 - i_position;
}

void OC5A(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR5A = ICR5 - i_position;
}

//ecriture de la position pour les pins OCnB
void OC1B(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR1B = ICR1 - i_position;
}

void OC3B(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR3B = ICR3 - i_position;
}

void OC4B(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR4B = ICR4 - i_position;
}

void OC5B(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR5B = ICR5 - i_position;
}


//ecriture de la position pour les pins OCnC
void OC1C(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR1C = ICR1 - i_position;
}

void OC3C(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR3C = ICR3 - i_position;
}

void OC4C(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR4C = ICR4 - i_position;
}

void OC5C(int i_degre)
{
  int i_position = translationDegreEnPosition(i_degre);
  OCR5C = ICR5 - i_position;
}


void initTimer1()
{
  TCCR1A |= 1<<WGM11 | 1<<COM1A1 | 1<<COM1A0 | 1<<COM1B1 | 1<<COM1B0 | 1<<COM1C1 | 1<<COM1C0;
  TCCR1B |= 1<<WGM13 | 1<<WGM12 | 1<<CS11;
  ICR1 = F_CPU/50;
}

void initTimer3()
{
  TCCR3A |= 1<<WGM31 | 1<<COM3A1 | 1<<COM3A0 | 1<<COM3B1 | 1<<COM3B0 | 1<<COM3C1 | 1<<COM3C0;
  TCCR3B |= 1<<WGM33 | 1<<WGM32 | 1<<CS31;
  ICR3 = F_CPU/50;
}

void initTimer4()
{
  TCCR4A |= 1<<WGM41 | 1<<COM4A1 | 1<<COM4A0 | 1<<COM4B1 | 1<<COM4B0 | 1<<COM4C1 | 1<<COM4C0;
  TCCR4B |= 1<<WGM43 | 1<<WGM42 | 1<<CS41;
  ICR4 = F_CPU/50;
}

void initTimer5()
{
  TCCR5A |= 1<<WGM51 | 1<<COM5A1 | 1<<COM5A0 | 1<<COM5B1 | 1<<COM5B0  | 1<<COM5C1 | 1<<COM5C0;
  TCCR5B |= 1<<WGM53 | 1<<WGM52 | 1<<CS51;
  ICR5 = F_CPU/50;
}
