#include <avr/io.h>
#include <util/delay.h>
#include "servo.h"
#define inclinaisonInit -50
#define nbAncien 80
#define nbWait 10

typedef enum bool bool;

enum bool
  {
    false,true
  };

bool isFinish = false;
int wait = 0;
float inclinaison_servo[12];

bool sens_rotation[6];

int inclinaison_init_droite = inclinaisonInit;
int inclinaison_init_gauche = - inclinaisonInit;
float ancienne_incliDroite[nbAncien]={inclinaisonInit};
float ancienne_incliGauche[nbAncien]={-inclinaisonInit};

int inclinaison_max = 40;
float pas = 2;
bool rot = true;


void init_sinusoide(void)
{ 
  inclinaison_servo[0] = inclinaison_init_droite;
  inclinaison_servo[1] = inclinaison_init_droite + (inclinaison_max *0.6);
  inclinaison_servo[2] = inclinaison_init_droite - (inclinaison_max *0.6);
  inclinaison_servo[3] = inclinaison_init_droite;
  inclinaison_servo[4] = inclinaison_init_droite + (inclinaison_max *0.6);
  inclinaison_servo[5] = inclinaison_init_droite - (inclinaison_max *0.6);
  
  inclinaison_servo[6] = inclinaison_init_gauche ;
  inclinaison_servo[7] = inclinaison_init_gauche - (inclinaison_max *0.6);
  inclinaison_servo[8] = inclinaison_init_gauche + (inclinaison_max *0.6);
  inclinaison_servo[9] = inclinaison_init_gauche;
  inclinaison_servo[10] = inclinaison_init_gauche - (inclinaison_max *0.6);
  inclinaison_servo[11] = inclinaison_init_gauche + (inclinaison_max *0.6);

  sens_rotation[0] = true;
  sens_rotation[1] = false;
  sens_rotation[2] = false;
  sens_rotation[3] = true;
  sens_rotation[4] = false;
  sens_rotation[5] = false;
  
}

void init_plat(void)
{
  inclinaison_servo[0] = inclinaison_init_droite;
  inclinaison_servo[1] = inclinaison_init_droite;
  inclinaison_servo[2] = inclinaison_init_droite;
  inclinaison_servo[3] = inclinaison_init_droite;
  inclinaison_servo[4] = inclinaison_init_droite;
  inclinaison_servo[5] = inclinaison_init_droite;
  
  inclinaison_servo[6] = inclinaison_init_gauche;
  inclinaison_servo[7] = inclinaison_init_gauche;
  inclinaison_servo[8] = inclinaison_init_gauche;
  inclinaison_servo[9] = inclinaison_init_gauche;
  inclinaison_servo[10] = inclinaison_init_gauche;
  inclinaison_servo[11] = inclinaison_init_gauche;

  sens_rotation[0] = true;
  sens_rotation[1] = true;
  sens_rotation[2] = true;
  sens_rotation[3] = true;
  sens_rotation[4] = true;
  sens_rotation[5] = true;

  
}

void UneOndulation(void){
  if(isFinish == false){
    if(inclinaison_servo[0] >= inclinaison_init_droite + inclinaison_max || inclinaison_servo[0] <= inclinaison_init_droite - inclinaison_max)
      {
	sens_rotation[0] = !sens_rotation[0];
      }
    if(sens_rotation[0])
      {
	inclinaison_servo[0] = inclinaison_servo[0] + pas;
	inclinaison_servo[6] = inclinaison_servo[6] - pas;
      }
    else
      {
	inclinaison_servo[0] = inclinaison_servo[0] - pas;
	inclinaison_servo[6] = inclinaison_servo[6] + pas;
      }
    if(inclinaison_servo[0] == inclinaison_init_droite && sens_rotation[0] == true){
      isFinish = true;
    }
  }
  else{
    if(wait != nbWait){
      wait ++;
    }
    else{
      isFinish = false;
      wait = 0;
    }
  }
  for(int i = nbAncien-1; i > 0; i--){
    ancienne_incliDroite[i] = ancienne_incliDroite[i-1];
    ancienne_incliGauche[i] = ancienne_incliGauche[i-1];
  }
  ancienne_incliDroite[0] = inclinaison_servo[0];
  ancienne_incliGauche[0] = inclinaison_servo[6];
  for(int i = 5; i > 0; i--){
    inclinaison_servo[i] = ancienne_incliDroite[(i+1)*nbAncien/6-1];
    inclinaison_servo[i+6] = ancienne_incliGauche[(i+1)*nbAncien/6-1];
  }
}

void setup(void)
{
  DDRB |= 0xE0;
  DDRE |= 0x38;
  DDRH |= 0x38;
  DDRL |= 0x38;
  initTimer1();
  initTimer3();
  initTimer4();
  initTimer5();

  //init_sinusoide();
  init_plat();
  
}

int main(void)
{
  setup();
   while (1)
    {
      UneOndulation();
      
      OC1C(inclinaison_servo[0]);
      OC1B(inclinaison_servo[1]);
      OC1A(inclinaison_servo[2]);
      OC4C(inclinaison_servo[3]);
      OC4B(inclinaison_servo[4]);
      OC4A(inclinaison_servo[5]);
      OC5B(inclinaison_servo[6]);
      OC5A(inclinaison_servo[7]);
      OC5C(inclinaison_servo[8]);
      OC3B(inclinaison_servo[9]);
      OC3C(inclinaison_servo[10]);
      OC3A(inclinaison_servo[11]);
      _delay_ms(3);
      }
}
