#include <avr/io.h>
#include <util/delay.h>

#define BAUDRATE 9600

void init_serial(void)
{
  //uint8_t baudrate = BAUDRATE;
  /* Set baud rate */
  UBRR0H = 0x25;
  UBRR0L = 0x80;

  /* Enable transmitter & receiver */
  UCSR0B = 0x18;

  /* Set 8 bits character and 1 stop bit */
  // UCSR0C = (1<<UCSZ01 | 1<<UCSZ00);

  /* Set off UART baud doubler */
  // UCSR0A &= ~(1 << U2X0);
}

void send_serial(unsigned char c)
{
  while(!(UCSR0A & 0x20))
  UDR0 = c;
}

unsigned char get_serial(void) {
 loop_until_bit_is_set(UCSR0A, RXC0);
  return UDR0;
}


int main(void)
{
  DDRA |= 0xFF;
  init_serial();
  char c;
  while(1)
    {
      c = get_serial();
      PORTA= c;
    }
}
